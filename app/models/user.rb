class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :ldap_authenticatable, :registerable,
       :recoverable, :rememberable, :trackable, :validatable

    validates :username, presence: true, uniqueness: true

    before_validation :get_ldap_email
    def get_ldap_email
      # logger.info Devise::LDAP::Adapter.get_ldap_entry(self.username)
      self.email  = self.username+'@ifrn.edu.br'
    end
end
