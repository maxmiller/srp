class Item < ActiveRecord::Base
  belongs_to :material
  mount_uploader :attach, AttachUploader
end
