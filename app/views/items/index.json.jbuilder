json.array!(@items) do |item|
  json.extract! item, :id, :company_name, :cnpj, :value, :website, :material_id
  json.url item_url(item, format: :json)
end
