json.array!(@materials) do |material|
  json.extract! material, :id, :user_id, :description, :specification, :status, :type_material, :amount, :finality, :note
  json.url material_url(material, format: :json)
end
