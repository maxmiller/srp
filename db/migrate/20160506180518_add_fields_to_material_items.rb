class AddFieldsToMaterialItems < ActiveRecord::Migration
  def change
    add_reference :material_items, :material, index: true, foreign_key: true
  end
end
