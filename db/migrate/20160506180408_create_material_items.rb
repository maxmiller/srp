class CreateMaterialItems < ActiveRecord::Migration
  def change
    create_table :material_items do |t|
      t.string :company_name
      t.string :cnpj
      t.decimal :value
      t.string :website

      t.timestamps null: false
    end
  end
end
