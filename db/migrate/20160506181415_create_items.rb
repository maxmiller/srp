class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :company_name
      t.string :cnpj
      t.string :value
      t.string :website
      t.belongs_to :material, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
