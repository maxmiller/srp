class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.references :users, index: true, foreign_key: true
      t.string :description
      t.string :specification
      t.integer :status
      t.integer :type_material
      t.integer :amount
      t.string :finality
      t.string :note

      t.timestamps null: false
    end
  end
end
